docker rm -f frontend-exercise       # remove old container
docker rmi frontend-exercise-image      # remove old image
docker build -t frontend-exercise-image .  # build new image
docker run -d --name frontend-exercise -p 80:80 frontend-exercise-image   # create and run new container
