# Use an official Node.js runtime as a parent image
FROM node:16.13.1 as node

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . .

# Install dependencies and build the app
RUN npm install
RUN npm run build --prod

# Use an official Nginx web server runtime as a parent image
FROM nginx:alpine

# Copy the built app from the previous stage into the container at /usr/share/nginx/html
COPY --from=node /app/dist /usr/share/nginx/html

# Start Nginx server
CMD ["nginx", "-g", "daemon off;"]
